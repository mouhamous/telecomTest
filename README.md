# Service Telecom Eligibility Test


## Requirements

* [Docker](http://www.docker.com/).
* [Docker Compose](http://docs.docker.com/compose/install/).
* [Poetry] (https://python-poetry.org/docs/).

## Backend local development

* create .env file and setup values
```
cp .env.copy .env
```

The `.env` file is the one that contains all your configurations and passwords, etc.


* Start the stack with Docker Compose:

```bash
docker-compose up -d 
```

* Now you can open your browser and interact with these URLs:

Frontend, built with Docker, with routes handled based on the path: http://localhost:8000

Backend, JSON based web API based on OpenAPI: http://localhost:8000/api/

Automatic interactive documentation with Swagger UI (from the OpenAPI api): http://localhost:8000/docs

Alternative automatic documentation with ReDoc (from the OpenAPI api): http://localhost:8000/redoc




**Note**: The first time you start your stack, it might take a minute for it to be ready. While the api waits for the database to be ready and configures everything. You can check the logs to monitor it.

To check the logs, run:

```bash
docker-compose logs
```

To check the logs of a specific service, add the name of the service, e.g.:

```bash
docker-compose logs api
```


## Backend local development, additional details


### Backend tests

To test the api run:

```
docker-compose exec api ./test.sh
```


### Migrations

As during local development your app directory is mounted as a volume inside the container, you can also run the migrations with `alembic` commands inside the container and the migration code will be in your app directory (instead of being only inside the container). So you can add it to your git repository.

Make sure you create a "revision" of your models and that you "upgrade" your database with that revision every time you change them. As this is what will update the tables in your database. Otherwise, your application will have errors.

* Start an interactive session in the api container:

```console
$ docker-compose exec api bash
```


* After changing a model (for example, adding a column), inside the container, create a revision, e.g.:

```console
$ alembic revision --autogenerate -m "Add column last_name to User model"
```

* Commit to the git repository the files generated in the alembic directory.

* After creating the revision, run the migration in the database (this is what will actually change the database):

```console
$ alembic upgrade head
```

If you don't want to use migrations at all, uncomment the line in the file at `./app/app/db/init_db.py` with:

```python
Base.metadata.create_all(bind=engine)
```

and comment the line in the file `prestart.sh` that contains:

```console
$ alembic upgrade head
```

`app/csv_to_json.py` is used to convert the csv to json data 

## secutity 
* Disable route documentation for production environment
