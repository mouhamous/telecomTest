import csv
import json


def csv_to_json(csv_file):
    operators_data = {}

    with open(csv_file, newline="") as csvfile:
        reader = csv.DictReader(csvfile, delimiter=";")
        for row in reader:
            operator_name = row["Operateur"]

            operator_data = {
                "x": int(row["x"]),
                "y": int(row["y"]),
                "two_g": int(row["2G"]),
                "three_g": int(row["3G"]),
                "four_g": int(row["4G"]),
            }
            if operator_name in operators_data:
                operators_data[operator_name].append(operator_data)
            else:
                operators_data[operator_name] = [operator_data]

    return operators_data


if __name__ == "__main__":
    csv_file_path = "datas.csv"
    json_data = csv_to_json(csv_file_path)

    with open("data.json", "w") as jsonfile:
        json.dump(json_data, jsonfile, indent=4)

    print("Conversion completed. JSON data saved to 'data.json'")
