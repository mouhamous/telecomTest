#!/bin/bash
# drop database and create
python /app/app/tests_pre_start.py

# Run migrations
alembic upgrade head

#load initial data
python /app/app/initial_data.py

# run test 
pytest 