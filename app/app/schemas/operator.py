from typing import Optional

from pydantic import BaseModel, ConfigDict


# Shared properties
class OperatorBase(BaseModel):
    name: Optional[str] = None


# Properties to receive via API on creation
class OperatorCreate(OperatorBase):
    id: int
    name: str


# Properties to receive via API on update
class OperatorUpdate(OperatorBase):
    ...


class OperatorInDBBase(OperatorBase):
    id: Optional[int] = None

    Config: ConfigDict = {"from_attributes": True}


# Additional properties to return via API
class Operator(OperatorInDBBase):
    ...
