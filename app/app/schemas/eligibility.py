from typing import Optional

from pydantic import BaseModel, ConfigDict


class EligibilityBase(BaseModel):
    operator_id: Optional[int]
    x: Optional[str] = None
    y: Optional[str] = None
    two_g: Optional[bool] = False
    three_g: Optional[bool] = False
    four_g: Optional[bool] = False


# Properties to receive via API on creation
class EligibilityCreate(EligibilityBase):
    operator_id: int


# Properties to receive via API on update
class EligibilityUpdate(EligibilityBase):
    ...


class EligibilityInDBBase(EligibilityBase):
    id: Optional[int] = None

    Config: ConfigDict = {"from_attributes": True}


# Additional properties to return via API
class Eligibility(EligibilityInDBBase):
    ...
