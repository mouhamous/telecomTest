from typing import Dict

from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from app import crud
from app.core.config import settings
from app.models import Eligibility


def test_find_nearest_point(db: Session):
    operator_id = 20815
    nearest_op1 = crud.eligibility.find_nearest_point_by_operator(
        db, 643185, 6872740, operator_id
    )
    assert nearest_op1.x == 642890
    assert nearest_op1.y == 6872907
    assert nearest_op1.operator_id == operator_id

    operator_id = 20801
    nearest_op2 = crud.eligibility.find_nearest_point_by_operator(
        db, 643185, 6872740, operator_id
    )
    assert nearest_op2.x == 642756
    assert nearest_op2.y == 6872960
    assert nearest_op2.operator_id == operator_id
