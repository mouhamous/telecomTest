from fastapi.testclient import TestClient

from sqlalchemy.orm import Session

from app.core.config import settings

from app.tests.utils.api_mocks import register_address_details


def test_get_eligibilities(client: TestClient, mock_requests, db: Session) -> None:

    address = "8+Boulevard+du+Port"

    register_address_details(mock_requests, address)

    r = client.get(
        f"{settings.API_V1_STR}/eligibilities?q={address}",
    )
    response = r.json()
    assert r.status_code == 200
    assert isinstance(response, list)
    for operator in response:
        data = list(operator.values())[0]
        assert isinstance(operator, dict)
        assert "2G" in data
        assert "3G" in data
        assert "4G" in data
