from typing import Dict, Generator

import pytest
import requests_mock

from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from app.db.session import SessionLocal
from main import app


@pytest.fixture(scope="session")
def db() -> Generator:
    yield SessionLocal()


@pytest.fixture(scope="module")
def client() -> Generator:
    with TestClient(app) as c:
        yield c


@pytest.fixture
def mock_requests():
    with requests_mock.Mocker() as mock:
        mock.real_http = True
        yield mock
