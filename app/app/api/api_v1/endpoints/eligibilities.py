import requests
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm import Session

from app import crud, schemas
from app.api import deps
from app.core.config import settings

router = APIRouter()


# @router.get("/", response_model=List[schemas.eligibility])
@router.get("/")
def read_details(
    q: str,
    db: Session = Depends(deps.get_db),
):
    r = requests.get(
        url=f"{settings.ADDRESS_API_URL}/search?q={q}",
    )
    porpeties = r.json()["features"][0]["properties"]
    # get all operators
    operators = crud.operator.get_operators(db)

    data = []
    for operator in operators:
        nearest = crud.eligibility.find_nearest_point_by_operator(
            db, porpeties["x"], porpeties["y"], operator.id
        )
        data.append(
            {
                operator.name: {
                    "2G": nearest.two_g,
                    "3G": nearest.three_g,
                    "4G": nearest.four_g,
                }
            }
        )
    return data
