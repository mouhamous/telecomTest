from fastapi import APIRouter

from app.api.api_v1.endpoints import eligibilities

api_router = APIRouter()

api_router.include_router(
    eligibilities.router, prefix="/eligibilities", tags=["eligibility"]
)
