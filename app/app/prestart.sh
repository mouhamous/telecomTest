#! /usr/bin/env bash

# drop db and recreate
python /app/app/tests_pre_start.py

# Run migrations
alembic upgrade head

# Create initial data in DB
python /app/app/initial_data.py