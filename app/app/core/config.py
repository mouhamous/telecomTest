import os
import secrets
from typing import Any, Dict, List, Optional, Union

from pydantic import (
    AnyHttpUrl,
    EmailStr,
    HttpUrl,
    AnyUrl,
    validator,
)
from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    API_V1_STR: str = "/api/v1"
    SECRET_KEY: str = secrets.token_urlsafe(32)
    BACKEND_CORS_ORIGINS: List[AnyHttpUrl] = []
    user: str = os.getenv("MYSQL_USER")
    password: str = os.getenv("MYSQL_PASSWORD")
    server: str = os.getenv("MYSQL_SERVER", "db")
    db: str = os.getenv("MYSQL_DATABASE")
    SQLALCHEMY_DATABASE_URI: str = f"mysql+pymysql://{user}:{password}@{server}/{db}"
    ADDRESS_API_URL: str = ""


settings = Settings()
