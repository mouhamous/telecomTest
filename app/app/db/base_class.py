from typing import Any

from sqlalchemy.ext.declarative import as_declarative, declared_attr

from sqlalchemy.orm import declarative_base, declared_attr


Base = declarative_base()
