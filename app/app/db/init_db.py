import json, asyncio
from typing import List

from sqlalchemy.orm import Session

from app import crud, schemas, models
from app.core.config import settings
from app.db import base  # noqa: F401

# make sure all SQL Alchemy models are imported (app.db.base) before initializing DB
# otherwise, SQL Alchemy might fail to initialize relationships properly
# for more details: https://github.com/tiangolo/full-stack-fastapi-postgresql/issues/28


async def init_db(db: Session) -> None:
    # Tables should be created with Alembic migrations
    # But if you don't want to use migrations, create
    # the tables un-commenting the next line
    # Base.metadata.create_all(bind=engine)
    operators = {"20801": "Orange", "20810": "SFR", "20815": "Free", "20820": "Bouygue"}
    data: list = json.load(open("./app/data/data.json"))
    for operator_id in data:
        # create operator
        operator_in = schemas.OperatorCreate(
            id=operator_id, name=operators[operator_id]
        )
        crud.operator.create(db, obj_in=operator_in)
        batch_size = 1000
        tasks = []
        # Collect the eligibility data into batches
        eligibility_batches: List[List[dict]] = [
            data[operator_id][i : i + batch_size]
            for i in range(0, len(data[operator_id]), batch_size)
        ]

        # Schedule the insertion of each batch of eligibilitys as an async task
        for eligibility_batch in eligibility_batches:
            tasks.append(
                asyncio.ensure_future(
                    create_eligibility(db, operator_id, eligibility_batch)
                )
            )

        # Wait for all the async tasks to complete
        await asyncio.gather(*tasks)


async def create_eligibility(
    db: Session, operator: str, eligibility_batch: List[dict]
) -> None:
    eligibility_objects = [
        models.Eligibility(**eligibility, operator_id=operator)
        for eligibility in eligibility_batch
    ]
    db.add_all(eligibility_objects)
    db.commit()
