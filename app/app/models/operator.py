from typing import TYPE_CHECKING

from sqlalchemy import Column, Integer, String

from app.db.base_class import Base


class Operator(Base):
    __tablename__ = "operator"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(String(25), unique=True)
