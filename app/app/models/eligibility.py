from typing import TYPE_CHECKING

from sqlalchemy import Boolean, Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from app.db.base_class import Base


class Eligibility(Base):
    __tablename__ = "eligibility"
    id = Column(Integer, primary_key=True, index=True)
    operator_id = Column(Integer, ForeignKey("operator.id"), index=True)
    x = Column(Integer, index=True, nullable=False)
    y = Column(Integer, index=True, nullable=False)
    two_g = Column(Boolean(), default=False)
    three_g = Column(Boolean(), default=False)
    four_g = Column(Boolean(), default=False)
