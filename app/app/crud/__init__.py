# For a new basic set of CRUD operations you could just do

# from .base import CRUDBase
# from app.models.item import Item
# from app.schemas.item import ItemCreate, ItemUpdate
from .crud_operator import operator
from .crud_eligibility import eligibility

# item = CRUDBase[Item, ItemCreate, ItemUpdate](Item)
