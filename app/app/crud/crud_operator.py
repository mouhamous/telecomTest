from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models.operator import Operator
from app.schemas.operator import OperatorCreate, OperatorUpdate


class CRUDOperator(CRUDBase[Operator, OperatorCreate, OperatorUpdate]):
    def get_operators(self, db: Session) -> list[Operator]:
        return db.query(Operator).all()


operator = CRUDOperator(Operator)
