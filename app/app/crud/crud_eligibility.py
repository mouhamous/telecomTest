from sqlalchemy.orm import Session
from sqlalchemy import func

from app.crud.base import CRUDBase
from app.models.eligibility import Eligibility
from app.schemas.eligibility import EligibilityCreate, EligibilityUpdate


class CRUDEligibility(CRUDBase[Eligibility, EligibilityCreate, EligibilityUpdate]):
    def find_nearest_point_by_operator(
        self, db: Session, x: int, y: int, operator_id: int
    ) -> Eligibility | None:

        return (
            db.query(Eligibility)
            .filter(Eligibility.operator_id == operator_id)
            .order_by(
                func.sqrt(
                    func.pow(Eligibility.x - x, 2) + func.pow(Eligibility.y - y, 2)
                )
            )
            .first()
        )


eligibility = CRUDEligibility(Eligibility)
